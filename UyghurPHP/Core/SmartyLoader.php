<?php  namespace UyghurPHP\Core;

class SmartyLoader{
	//save smartys load stat
	private static $smarty = NULL;
	//construct function
	public function __construct(){
		//check the smarty is run
		if(is_null(self::$smarty)) return;
		//run smarty
		$smarty = new \Smarty;
		$smarty->template_dir = APP_NAME.'/'.MODULE."/View/".CONTROLLER;
		//compile dir
		$path = "Storage/Compile".MODULE.'/'.CONTROLLER;
		is_dir($path) ||mkdir($path,0777,true);
		$smarty->compile_dir = $path;
		//cache dir
		$path = "Storage/Cache".MODULE.'/'.CONTROLLER;
		is_dir($path) ||mkdir($path,0777,true);
		$smarty->cache_dir = $path;
		//caching on
		$smarty->caching = false;
		//caching lifetime;
		$smarty->caching_lifetime = 10;
		//left and right delimiters
		$smarty->left_delimiter = "{{";
		$smarty->right_delimiter ="}}";
		//smarty nocache
		//nocache是一个函数，放在functions.php
		$smarty->register_block("nocache", "nocache", false);

		//把Smarty对象赋给静态属性
		self::$smarty = $smarty;
	}
	//自定义display方法，目的就是为了引用Smarty里面的Display方法
	protected function display($tpl=NULL){
		//如果以用户没有传模板参数，则找方法名一样的模板文件
		if(is_null($tpl)) $tpl = ACTION .'.php';
		self::$smarty->display($tpl,$_SERVER['REQUEST_URI']);
	}
	//自定义的assign
	protected function assign($var,$value){
		//调用smarty里面的assign
		self::$smarty->assign($var,$value);
	}
	
	//自定义的
	protected function is_cached($tpl){
		//调用smarty里面的is_cached
		return self::$smarty->is_cached($tpl,$_SERVER['REQUEST_URI']);
	}
	
	//清除缓存
	protected function clear_cache($tpl){
		return self::$smarty->clear_cache($tpl,$_SERVER['REQUEST_URI']);
	}
}


 ?>