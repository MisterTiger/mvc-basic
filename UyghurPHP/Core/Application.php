<?php  namespace UyghurPHP\Core;

class Application{
	
	public static function run(){
		//run default paramiter
		self::init();
		//register auto load function 
		spl_autoload_register(array(__CLASS__,'auto'));
		//run app
		self::appRun();
	}

	//init
	private static function init(){
		//session start
		session_id() || session_start();
		//default time zone
		date_default_timezone_set('PRC');
	}
	//auto action for spl_autoload_register
	private static function auto($className){
		//is controller
		if(substr($className, -10)=='Controller'){
			$path = APP_NAME.'/'.str_replace('\\', '/', $className).'.class.php';
		}else{
			//is tool
			$path = APP_NAME.'.'.str_replace('\\', '/', $className).'.php';
		}
		//if not find this file
		if(!is_file($path)){
			halt('文件'.$path.'不存在');
		}
		//and require it
		require $path;
	}

	//run app action
	private static function appRun(){
		//module
		$m = isset($_GET['m'])?ucfirst($_GET['m']):'Home';
		define('MODULE', $m);
		//Controller
		$c = isset($_GET['c'])?ucfirst($_GET['c']):'Index';
		define('CONTROLLER', $c);
		//action
		$action = isset($_GET['a'])?$_GET['a']:'index';
		define('ACTION', $action);
		//$name = \Home\Controller\IndexController
		$name = "\\{$m}\Controller\{$c}Controller";

		header('Content-type:text/html;charset=utf-8');
		echo "<div style='width:100%;height:60px;line-height:60px;font-size:20px;color:#fff;position:fixed;bottom:0;left:0;background:#34495e;text-align:center;font-family:Verdana'>项目名称为".APP_NAME."现在是".MODULE."模块".CONTROLLER."控制器的".ACTION."方法</div>";

		$app = new $name;
		$app->$action();
	}
}
	
 ?>