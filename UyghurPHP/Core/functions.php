<?php

    header('Content-type:text/html;charset=utf-8');
    //打印函数
    function p($var){
         echo '<pre style="padding:10px;background:#D9EDF7;border:1px solid #BCE8F1;color:#31708F">';
            print_r($var);
         echo "</pre>";
    }

    /**
     * [success 成功提示函数]
     * @param  [type] $msg [成功提示的消息]
     * @param  [type] $url [成功之后跳转的地址]
     * @return [type]      [description]
     */
    function success($msg,$url){
        $str = <<<str
<script>
alert('{$msg}');
location.href='{$url}';
</script>
str;
        echo $str;exit;
    }

//局部不缓存，在SmartView里面有体现
function nocache($params, $content, &$smarty){
    return $content;
}
//模型方法
function M(){
    $model = new \LemonPHP\Tool\Model;
    return $model;
}
//错误终止函数
function halt($msg){
    include 'Public/halt.html';
    exit();
}
?>