<?php 
	class Uyghur{
		public static function run(){
			if(!is_dir(APP_NAME)){
				self::createDir();
				self::copyFile();
			}
			self::loadCore();
			\UyghurPHP\Core\Application::run();
		}
		//create directory
		private static function createDir(){
			$dirArray = array(
					//default controller
					APP_NAME.'/Home/Controller',
					//default view path
					APP_NAME.'/Home/View/Index',
					//public folder
					'Public',
				);
			foreach ($dirArray as $dir) {
				is_dir($dir) || mkdir($dir,0777,true);
			}
		}
		//copy default files
		private static function copyFile(){
			//copy default IndexController
			copy('UyghurPHP/Common/IndexController.class.php', APP_NAME.'/Home/Controller/IndexController.class.php');
			//copy default view file
			copy('UyghurPHP/Common/index.php', APP_NAME.'/Home/View/Index/index.php');
			//copy error file
			copy('UyghurPHP/Common/halt.html', 'Public/halt.html');
		}
		//loadCore
		private static function loadCore(){
			//load functions
			require 'UyghurPHP/Core/functions.php';
			//load Smarty class
			require 'UyghurPHP/Org/Smarty/Smarty.class.php';
			//load smartyloader
			require 'UyghurPHP/Core/SmartyLoader.php';
			//load default controller
			require 'UyghurPHP/Core/Controller.php';
			//load framework file
			require 'UyghurPHP/Core/Application.php';
		}
	}
	Uyghur::run();
 ?>